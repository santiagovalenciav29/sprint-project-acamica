const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Sprint project 2 - Protalento",
            version: "1.0.0",
            description: "Proyecto 2 para acamica DWBE Persiste",
            contact : {
                name : " Santiago Valencia Valencia",
                email : "santiagovalenciav29@gmail.com"
            }
        },
        servers: [
            {
                url: "http://localhost:5000",
                description: "Servidor de prueba"
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [
            {
                bearerAuth: []
            }
        ]
    },
    apis: ["./src/routes/*.js"]
};

module.exports = swaggerOptions;
